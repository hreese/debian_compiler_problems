CFLAGS=-Wall -g -fsanitize=address
LDFLAGS=-lsodium

.PHONY: clean

test: test.c
	gcc   $(CFLAGS) -o $@.gcc   $< $(LDFLAGS)
	clang $(CFLAGS) -o $@.clang $< $(LDFLAGS)

clean:
	rm -f test
